import { loadConfig } from 'env-decorator';
import { ObjectFactory } from 'typescript-ioc';
import EnvironmentService from '@services/environment';

export const environmentFactory: ObjectFactory = () => {
  const config = loadConfig(EnvironmentService);

  const service = new EnvironmentService();

  return Object.assign(service, config);
}