import {
  Controller,
  Route,
  Get,
  Path,
  Post,
  Body,
  SuccessResponse,
  Put,
  Delete
} from 'tsoa';
import { CREATED, OK, NO_CONTENT } from 'http-status-codes';
import { Inject } from 'typescript-ioc';
import { IProductCreateRequest, IProductGetResponse, IProductGetAllResponse, IProductUpdateResponse, IProductCreateResponse, IProductUpdateRequest, IProductDeleteRequest, IProductCheckSkuResponse, IProductCheckSkuRequest } from '@interfaces/product';
import ProductsService from '@services/products';
import { IRequestError } from '../../interfaces';

@Route('products')
export class ProductsController extends Controller {
  @Inject private service!: ProductsService;

  @Get('{id}')
  public async get(@Path() id: number): Promise<IProductGetResponse | IRequestError> {
    try {
      const data = await this.service.get(id);

      if (data) {
        return {
          data
        }
      } else {
        throw Error('RIP');
      }
    } catch ({ message }) {
      return {
        error: {
          message
        }
      }
    }
  }

  @Post('sku')
  public async checkSku(@Body() request: IProductCheckSkuRequest): Promise<IProductCheckSkuResponse | IRequestError> {
    try {
      const data = await this.service.skuExists(request?.data?.sku);

      if (data !== undefined) {
        return {
          data: {
            inUse: data
          }
        }
      } else {
        throw Error('RIP');
      }
    } catch ({ message }) {
      return {
        error: {
          message
        }
      }
    }
  }

  @Get()
  public async getAll(): Promise<IProductGetAllResponse | IRequestError> {
    try {
      const data = await this.service.getAll();

      if (data) {
        return {
          data
        }
      } else {
        throw Error('RIP');
      }
    } catch ({ message }) {
      return {
        error: {
          message
        }
      }
    }
  }

  @SuccessResponse(CREATED, 'Created')
  @Post()
  public async create(@Body() request: IProductCreateRequest): Promise<IProductCreateResponse | IRequestError> {
    try {
      const data = await this.service.create(request?.data);

      if (data) {
        this.setStatus(CREATED);

        return {
          data
        }
      } else {
        throw Error('RIP');
      }
    } catch ({ message }) {
      return {
        error: {
          message
        }
      }
    }
  }

  @SuccessResponse(OK, 'Updated')
  @Put('{id}')
  public async update(@Path() id: number, @Body() request: IProductUpdateRequest): Promise<IProductUpdateResponse | IRequestError> {
    try {
      const data = await this.service.update(id, request?.data);

      if (data) {
        this.setStatus(OK);

        return {
          data
        }
      } else {
        throw Error('RIP');
      }
    } catch ({ message }) {
      return {
        error: {
          message
        }
      }
    }
  }

  @SuccessResponse(NO_CONTENT, 'Deleted')
  @Delete()
  public async delete(@Body() request: IProductDeleteRequest): Promise<void | IRequestError> {
    try {
      const data = await this.service.delete(request?.data?.id);

      if (!data) {
        throw Error('Item not found.');
      }

      this.setStatus(NO_CONTENT);
    } catch ({ message }) {
      return {
        error: {
          message
        }
      }
    }
  }
}