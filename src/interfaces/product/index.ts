export interface IProduct {
  id: number;
  sku: string;
  name: string;
  description: string;
  price: any;
  quantity: number;
  imageSrc?: string;
}

export interface IProductGetResponse {
  data: IProduct;
}

export interface IProductGetAllResponse {
  data: IProduct[];
}

export interface IProductCreateRequest {
  data: Omit<IProduct, 'id'>;
}

export interface IProductCreateResponse {
  data: IProduct;
}

export interface IProductUpdateRequest {
  data: Partial<IProduct>;
}

export interface IProductUpdateResponse {
  data: IProduct;
}

export interface IProductDeleteRequest {
  data: Pick<IProduct, 'id'>;
}

export interface IProductCheckSkuRequest {
  data: Pick<IProduct, 'sku'>;
}

export interface IProductCheckSkuResponse {
  data: {
    inUse: boolean;
  }
}
