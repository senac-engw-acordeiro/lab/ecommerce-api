import { Singleton, Inject } from 'typescript-ioc';
import { createConnection, Connection } from 'typeorm';
import EnvironmentService from '@services/environment';

@Singleton
export default class ConnectionService {
  private connection?: Connection;
  @Inject private environment!: EnvironmentService;

  constructor() {
    this.createConnection();
  }

  public async getConnection() {
    if (!(this.connection instanceof Connection)) {
      await this.createConnection();
    }

    return this.connection;
  }

  private async connect() {
    if (this.connection instanceof Connection && !this.connection?.isConnected) {
      await this.connection.connect();
    }
  }

  private async createConnection(connect = true) {
    const options = this.environment.getDbConnectionOptions();
    const connection = await createConnection(options);

    this.connection = connection;
  }
}