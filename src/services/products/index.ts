import { Inject } from 'typescript-ioc';
import { IProduct } from '@interfaces/product';
import ConnectionService from '@services/connection';
import Product from '@entities/product';
import { AtLeastOne } from '../../interfaces';

export default class ProductsService {
  @Inject private connectionService!: ConnectionService;

  private async getRepository() {
    const connection = await this.connectionService.getConnection();

    return connection?.getRepository(Product);
  }

  public async get(id: number) {
    const repository = await this.getRepository();

    return await repository?.findOne(id);
  }

  public async getAll() {
    const repository = await this.getRepository();

    return await repository?.find();
  }

  public async create(item: Omit<IProduct, 'id'>) {
    const repository = await this.getRepository();

    if (await this.skuExists(item.sku)) {
      throw Error('SKU already exists.');
    }

    const result = await repository?.insert(item);

    return this.get(result?.identifiers[0]?.id);
  }

  public async update(id: number, { id: possibleBodyId, ...rest }: AtLeastOne<IProduct>) {
    const repository = await this.getRepository();

    await repository?.update(id, rest);

    return await this.get(id);
  }

  public async delete(id: number) {
    const repository = await this.getRepository();

    const result = await repository?.delete(id);

    return !!result;
  }

  public async skuExists(sku: string) {
    const repository = await this.getRepository();

    const result = await repository?.findOne({
      select: ['sku'],
      where: {
        sku
      }
    });

    return !!result;
  }
}
