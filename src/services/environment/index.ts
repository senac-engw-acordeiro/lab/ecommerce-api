import { Env } from 'env-decorator'
import { ConnectionOptions } from 'typeorm';
import { Singleton, Factory } from 'typescript-ioc';
import { environmentFactory } from '@factories/environment';

export interface IEnvironment {
  dbHost: string;
  dbPort: number;
  dbName: string;
  dbUser: string;
  dbSecret: string;
  dbType: string;
}

@Singleton
@Factory(environmentFactory)
export default class EnvironmentService implements IEnvironment {
  @Env('DB_HOST') public dbHost!: string;
  @Env('DB_PORT', { type: 'number' }) public dbPort!: number;
  @Env('DB_NAME') public dbName!: string;
  @Env('DB_USER') public dbUser!: string;
  @Env('DB_SECRET') public dbSecret!: string;
  @Env('DB_TYPE') public dbType!: string;

  public getDbConnectionOptions(): ConnectionOptions {
    return {
      type: this.dbType as any,
      host: this.dbHost,
      port: this.dbPort,
      username: this.dbUser,
      password: this.dbSecret,
      database: this.dbName,
      synchronize: true,
      entities: ['src/entities/**/index.ts']
    }
  }
}
