    // tslint:disable: unified-signatures
import { Entity, Column, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';
import { IProduct } from '@interfaces/product';

@Entity()
export default class Product implements IProduct {
  @PrimaryGeneratedColumn() public id: number;
  @Column({ unique: true }) public sku: string;
  @Column() public name: string;
  @Column() public description: string;
  @Column() public price: number;
  @Column() public quantity: number;
  @Column({ nullable: true }) public imageSrc?: string;

  constructor();
  constructor(data: IProduct | Partial<IProduct>);
  constructor(data?: any) {
    this.id = data?.id;
    this.sku = data?.sku;
    this.name = data?.name;
    this.description = data?.description;
    this.price = data?.price;
    this.quantity = data?.quantity;
    this.imageSrc = data?.imageSrc;
  }
}
