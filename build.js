const fs = require('fs-extra');
const childProcess = require('child_process');

try {
    fs.removeSync('./dist/');
    childProcess.exec('tsoa routes -c tsoa-config.json');
    childProcess.exec('tsc --build tsconfig.prod.json');
} catch (err) {
    console.log(err);
}
